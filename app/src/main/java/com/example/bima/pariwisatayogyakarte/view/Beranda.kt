package com.example.bima.pariwisatayogyakarte.view


import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.bima.pariwisatayogyakarte.R
import com.example.bima.pariwisatayogyakarte.adapter.PariwisataYogyakarteAdapter
import com.example.bima.pariwisatayogyakarte.getdata.Antarmuakamu
import com.example.bima.pariwisatayogyakarte.getdata.Yogyakarte
import com.example.bima.pariwisatayogyakarte.helper.BubleHelp
import com.example.bima.pariwisatayogyakarte.helper.RetrofitHelper
import com.example.bima.pariwisatayogyakarte.model.DataPariwisata
import com.example.bima.pariwisatayogyakarte.model.GetorPostData
import com.example.bima.pariwisatayogyakarte.model.MyData
import com.roger.catloadinglibrary.CatLoadingView
import kotlinx.android.synthetic.main.activity_beranda.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity
import retrofit2.Call

class Beranda : AppCompatActivity(),Antarmuakamu {

    private lateinit var adapter: PariwisataYogyakarteAdapter
    private lateinit var retrofit: GetorPostData
    private lateinit var getData: Call<DataPariwisata>
    private lateinit var yogyakarte :Yogyakarte
    private var datas: MutableList<MyData> = mutableListOf()

    private var cat = CatLoadingView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beranda)

        cat.show(supportFragmentManager,"show")


        retrofit = RetrofitHelper.retrofit().create(GetorPostData::class.java)
        getData = retrofit.listPariwisata()

        yogyakarte = Yogyakarte(this, retrofit)

        yogyakarte.getDataKate(getData)

        adapter = PariwisataYogyakarteAdapter(datas){
            cat.show(supportFragmentManager,"show")
            GlobalScope.launch {
                delay(4000)
                ctx.startActivity<MapsActivity>("all" to it)
            }
        }

        recycler.adapter = adapter
        recycler.layoutManager = LinearLayoutManager(applicationContext)

    }


    override fun onStart() {
        super.onStart()
        GlobalScope.launch {
            delay(2000)
            cat.dismiss()
        }

    }

    override fun getDataKate(data: List<MyData>) {
        datas.clear()
        datas.addAll(data)
        adapter.notifyDataSetChanged()
    }
}
