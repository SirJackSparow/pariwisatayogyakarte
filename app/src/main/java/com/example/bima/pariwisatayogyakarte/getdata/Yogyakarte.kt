package com.example.bima.pariwisatayogyakarte.getdata
import com.example.bima.pariwisatayogyakarte.helper.RetrofitHelper
import com.example.bima.pariwisatayogyakarte.model.DataPariwisata
import com.example.bima.pariwisatayogyakarte.model.GetorPostData
import com.example.bima.pariwisatayogyakarte.model.MyData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class Yogyakarte(private val view:Antarmuakamu, private var retrofit: GetorPostData) {


    fun getDataKate(getData: Call<DataPariwisata>){
        retrofit = RetrofitHelper.retrofit().create(GetorPostData::class.java)
        getData.enqueue(object : Callback<DataPariwisata> {
            override fun onFailure(call: Call<DataPariwisata>, t: Throwable) {

            }

            override fun onResponse(call: Call<DataPariwisata>, response: Response<DataPariwisata>) {
                var data = response.body()!!.data
                view.getDataKate(data)
            }

        })
    }
}

interface Antarmuakamu{
   fun getDataKate(data: List<MyData>)
}