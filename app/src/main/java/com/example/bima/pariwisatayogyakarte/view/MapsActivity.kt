package com.example.bima.pariwisatayogyakarte.view

import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder
import com.example.bima.pariwisatayogyakarte.R
import com.example.bima.pariwisatayogyakarte.helper.BubleHelp
import com.example.bima.pariwisatayogyakarte.helper.SharedPreference
import com.example.bima.pariwisatayogyakarte.helper.database
import com.example.bima.pariwisatayogyakarte.model.Favorite
import com.example.bima.pariwisatayogyakarte.model.MyData
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.roger.catloadinglibrary.CatLoadingView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_maps.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import java.io.IOException

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var model:MyData
    var namaPa: String? = null
    var alamatPa: String? = null
    var deskPa: String? = null
    private var isFavorite: Boolean = false
    var image:String? = null
    var addressList : List<Address>? = null
    private var menuItem: Menu? = null
    private lateinit var buble: BubbleShowCaseBuilder

    private var cat = CatLoadingView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val intent = intent
        model = intent.getParcelableExtra("all")
        namaPa = model.nama_pariwisata
        alamatPa = model.alamat_pariwisata
        deskPa = model.detail_pariwisata
        image = model.gambar_pariwisata

        checkFavorite()
        Picasso.get().load(image).into(img)
        nama.text = namaPa
        alamat.text = alamatPa
        desk.text = deskPa
    }

    override fun onMapReady(googleMap: GoogleMap) {
        cat.show(supportFragmentManager,"show")
        mMap = googleMap
        val geocoder = Geocoder(this)
        try {
            addressList = geocoder.getFromLocationName(namaPa,1)
        }catch(e: IOException) {
            e.printStackTrace()
        }
        val addreass = addressList?.get(0)

        if(addreass == null){
            Toast.makeText(this,"Check Koneksi",Toast.LENGTH_LONG).show()
            // Add a marker in Sydney and move the camera
            val namaTempat = LatLng(33.8688, 151.2093)
            mMap.addMarker(MarkerOptions().position(namaTempat).title(namaPa))
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(namaTempat))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(namaTempat, 16f))
        }else{
            // Add a marker in Sydney and move the camera
            val namaTempat = LatLng(addreass.latitude, addreass.longitude)
            mMap.addMarker(MarkerOptions().position(namaTempat).title(namaPa))
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(namaTempat))
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(namaTempat, 16f))
            mMap.setOnInfoWindowClickListener {
                startActivity(Intent(this,Beranda::class.java))
            }
            cat.dismiss()
        }

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_fav, menu)
        menuItem = menu
        setFavorites()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId){
            R.id.add_to_scedulefavorite -> {
                var pref = SharedPreference(this)
                if (pref.fav() == 0){
                    buble = BubleHelp.getSimpleChase(this,"Lihat List Favorite di Tombol Tempat Favorite di halaman profil","Hello").targetView(findViewById(R.id.add_to_scedulefavorite))
                    buble.show()
                    pref.setfav(1)
                }
                if (isFavorite) remove() else addFavorite()
                isFavorite = !isFavorite
                setFavorites()
                true
            }else -> super.onOptionsItemSelected(item)
        }

    }

    private fun setFavorites(){
        if (isFavorite){
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        }else{
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this,R.drawable.ic_add_to_favorites)
        }
    }

    private fun addFavorite(){
        try {
            database.use {
                insert(Favorite.TABLE_NAME,
                        Favorite.NAMA to namaPa,
                    Favorite.ALAMAT to alamatPa,
                    Favorite.DETAIL to deskPa,
                    Favorite.GAMBAR to image)
            }
        }catch (e: SQLiteConstraintException){}
    }

    private fun remove(){
        try {
            database.use {
                delete(Favorite.TABLE_NAME, "(NAMA) = {nama}","nama" to model.nama_pariwisata)
            }
        }catch (e : SQLiteConstraintException){}
    }

    private fun checkFavorite():Boolean{
        database.use {
            val result = select(Favorite.TABLE_NAME)
                .whereArgs("(NAMA = {nama})","nama" to model.nama_pariwisata)
            val favorite = result.parseList(classParser<Favorite>())
            if (!favorite.isEmpty())
                isFavorite = true
        }
        return isFavorite
    }



}
