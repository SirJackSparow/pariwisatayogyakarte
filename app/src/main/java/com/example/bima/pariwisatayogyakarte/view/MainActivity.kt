package com.example.bima.pariwisatayogyakarte.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder
import com.example.bima.pariwisatayogyakarte.R
import com.example.bima.pariwisatayogyakarte.helper.BubleHelp
import com.example.bima.pariwisatayogyakarte.helper.CheckConnection
import com.example.bima.pariwisatayogyakarte.helper.SharedPreference
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.roger.catloadinglibrary.CatLoadingView
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {

    private lateinit var mDatabaseRef: DatabaseReference
    private lateinit var mDatabase: FirebaseDatabase
    private lateinit var mAuth: FirebaseAuth
    private lateinit var buble: BubbleShowCaseBuilder


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseRef = mDatabase.reference.child("Users")
        mAuth = FirebaseAuth.getInstance()
        checkFirstime()

        goFav.setOnClickListener {
                ctx.startActivity<FavoriteList>()
        }
    }

    private fun checkFirstime(){
        var pref = SharedPreference(this)
        if (pref.firstTimeApp() == 0){
            buble = BubleHelp.getSimpleChase(this,"Click Gambar Home di atas untuk ke list tempat wisata","Hello").targetView(images)
            buble.show()
            pref.setfirstTimeApp(1)
        }
    }

    override fun onStart() {
        super.onStart()
        val userReference = mDatabaseRef.child(mAuth.currentUser!!.uid)
        email.text = mAuth.currentUser!!.email
        if (!mAuth.currentUser!!.isEmailVerified){
            emailver.visibility = View.VISIBLE
        }else{
            emailver.visibility = View.GONE
        }
        userReference.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }
            override fun onDataChange(p0: DataSnapshot) {
               nama?.text = p0.child("nama").value as String
               alamat?.text = p0.child("alamat").value as String
            }

        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.

        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                if (CheckConnection.isNetworkOnline(this)) {
                    mAuth.signOut()
                    startActivity(Intent(this, Login::class.java))
                } else {
                    Toast.makeText(this, "Check Koneksi Anda", Toast.LENGTH_LONG).show()
                }
                true
            }
             R.id.home -> {
                 ctx.startActivity<Beranda>()
                 true
             }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
