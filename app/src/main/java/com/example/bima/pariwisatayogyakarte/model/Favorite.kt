package com.example.bima.pariwisatayogyakarte.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Favorite(val id:Long?,val nama: String?,val alamat: String?,val detail :String?,val gambar: String?):Parcelable{
    companion object {
        const val TABLE_NAME: String = "FAVORITE"
        const val ID: String = "ID_"
        const val NAMA: String = "NAMA"
        const val ALAMAT:String = "ALAMAT"
        const val DETAIL:String = "DETAIL"
        const val GAMBAR:String = "GAMBAR"
    }
}