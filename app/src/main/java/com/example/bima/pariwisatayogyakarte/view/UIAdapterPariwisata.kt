package com.example.bima.pariwisatayogyakarte.view

import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.bima.pariwisatayogyakarte.R
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class UIAdapterPariwisata : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {

        cardView {
            elevation = 4f
            lparams(matchParent, wrapContent) {
                margin = dip(5)
            }
            linearLayout {
                lparams(matchParent, wrapContent)
                padding = dip(16)
                orientation = LinearLayout.HORIZONTAL

                imageView {
                    id = R.id.imageP
                }.lparams {
                    width = dip(100)
                    height = dip(100)
                }

                linearLayout {
                    lparams(matchParent, wrapContent)
                    padding = dip(15)
                    orientation = LinearLayout.VERTICAL

                    textView {
                        id = R.id.namaP
                    }.lparams {
                        marginStart = dip(5)
                        weight = 10f
                    }

                    textView {
                        id = R.id.alamatP
                        textSize = 17f
                    }.lparams {
                        marginStart = dip(5)
                    }

                }
            }
        }

    }
}