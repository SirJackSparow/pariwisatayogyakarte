package com.example.bima.pariwisatayogyakarte.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import com.example.bima.pariwisatayogyakarte.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.forgotpassword.*

class ForgotPassword : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgotpassword)
        mAuth = FirebaseAuth.getInstance()
        send.setOnClickListener {
            sendEmail()
        }
    }

    private fun sendEmail(){
        var email = email.text.toString()

        if (!TextUtils.isEmpty(email)) {
          mAuth.sendPasswordResetEmail(email)
              .addOnCompleteListener{task ->
                  if (task.isSuccessful){
                      Toast.makeText(this,"email send",Toast.LENGTH_LONG).show()
                      startActivity(Intent(this,Login::class.java))
                      finish()
                  }else{
                      Toast.makeText(this,"No user Founded",Toast.LENGTH_LONG).show()
                  }
              }
        }
        else{
            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show()
        }
    }
}