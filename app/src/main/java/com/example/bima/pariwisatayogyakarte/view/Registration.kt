package com.example.bima.pariwisatayogyakarte.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.example.bima.pariwisatayogyakarte.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.roger.catloadinglibrary.CatLoadingView
import kotlinx.android.synthetic.main.registration.*

class Registration : AppCompatActivity(){

    private var mDatabaseReference: DatabaseReference? = null
    private var mDatabase: FirebaseDatabase? = null
    private var mAuth: FirebaseAuth? = null
    private val TAG = "Registration"
    private  var cat : CatLoadingView = CatLoadingView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registration)
        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Users")
        mAuth =  FirebaseAuth.getInstance()



        registrasi.setOnClickListener {
            createNewAccount()
        }
    }

    private fun createNewAccount() {
        cat.show(supportFragmentManager,"show")
        var namaLengkap = nama.text.toString()
        var alamat = alamat.text.toString()
        var email = email.text.toString()
        var pwd =pwd.text.toString()

        if (pwd.length < 7){
            Toast.makeText(this,"Password Terlalu Pendek",Toast.LENGTH_SHORT).show()
        }else{
            mAuth!!
                .createUserWithEmailAndPassword(email,pwd)
                .addOnCompleteListener(this){ task ->

                    if (task.isSuccessful){
                        Log.d(TAG, "createUserWithEmail:success")

                        val userId = mAuth!!.currentUser!!.uid
                       peripikasiEmail()
                        var currentUserDB = mDatabaseReference!!.child(userId)
                        currentUserDB.child("nama").setValue(namaLengkap)
                        currentUserDB.child("alamat").setValue(alamat)

                        val intent = Intent(this, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    }else{
                        Log.w(TAG, "createUserWithEmail:failure", task.exception)
                        Toast.makeText(this, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                        cat.dismiss()
                    }
                }
        }

    }

    private fun peripikasiEmail() {
        val mUser = mAuth!!.currentUser
        mUser!!.sendEmailVerification()
            .addOnCompleteListener(this){task ->
                if (task.isSuccessful) {
                    Toast.makeText(this,
                        "Verification email sent to " + mUser.getEmail(),
                        Toast.LENGTH_SHORT).show()
                } else {
                    Log.e(TAG, "sendEmailVerification", task.exception)
                    Toast.makeText(this,
                        "Failed to send verification email.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }
}