package com.example.bima.pariwisatayogyakarte.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.example.bima.pariwisatayogyakarte.R
import com.example.bima.pariwisatayogyakarte.model.Favorite
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class AdapterFavorite (private val favorite: List<Favorite>,private val listener: (Favorite)->Unit)
    : RecyclerView.Adapter<ViewHolderr>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolderr {
        return ViewHolderr(UIAdapterFavorite().createView(AnkoContext.create(p0.context,p0)))
    }

    override fun getItemCount(): Int {
        return favorite.size
    }

    override fun onBindViewHolder(p0: ViewHolderr, p1: Int) {
        p0.bindItem(favorite[p1],listener)
    }
}

class ViewHolderr(view: View): RecyclerView.ViewHolder(view){
    val namaP : TextView = view.find(R.id.namaPe)
    val alamatP: TextView = view.find(R.id.alamatPe)
    val imageP: ImageView = view.find(R.id.imagePe)
    fun bindItem(favorite: Favorite, listener: (Favorite) -> Unit){
       Picasso.get().load(favorite.gambar).into(imageP)
        namaP.text = favorite.nama
        alamatP.text = favorite.alamat
    }
}

class UIAdapterFavorite : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui) {

        cardView {
            elevation = 4f
            lparams(matchParent, wrapContent) {
                margin = dip(5)
            }
            linearLayout {
                lparams(matchParent, wrapContent)
                padding = dip(16)
                orientation = LinearLayout.HORIZONTAL

                imageView {
                    id = R.id.imagePe
                }.lparams {
                    width = dip(100)
                    height = dip(100)
                }

                linearLayout {
                    lparams(matchParent, wrapContent)
                    padding = dip(15)
                    orientation = LinearLayout.VERTICAL

                    textView {
                        id = R.id.namaPe
                    }.lparams {
                        marginStart = dip(5)
                        weight = 10f
                    }

                    textView {
                        id = R.id.alamatPe
                        textSize = 17f
                    }.lparams {
                        marginStart = dip(5)
                    }

                }
            }
        }

    }
}