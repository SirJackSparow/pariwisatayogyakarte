package com.example.bima.pariwisatayogyakarte.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MyData(val nama_pariwisata: String,val alamat_pariwisata:String, val detail_pariwisata:String,val gambar_pariwisata:String):Parcelable