package com.example.bima.pariwisatayogyakarte.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.bima.pariwisatayogyakarte.R
import com.example.bima.pariwisatayogyakarte.model.MyData
import com.example.bima.pariwisatayogyakarte.view.UIAdapterPariwisata
import com.squareup.picasso.Picasso
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.find

class PariwisataYogyakarteAdapter(private val dataList: List<MyData>, private val listener: (MyData) -> Unit)
    :RecyclerView.Adapter<MyHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        return MyHolder(UIAdapterPariwisata().createView(AnkoContext.create(parent.context,parent)))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
       holder.bindItem(dataList[position],listener)
    }

}

class MyHolder(view: View): RecyclerView.ViewHolder(view){
    val namaP: TextView = view.find(R.id.namaP)
    val image : ImageView = view.find(R.id.imageP)
    val alamat : TextView = view.find(R.id.alamatP)

    fun bindItem(data: MyData,listener: (MyData) -> Unit){
        Picasso.get().load(data.gambar_pariwisata).into(image)
        namaP.text = data.nama_pariwisata
        alamat.text = data.alamat_pariwisata
        itemView.setOnClickListener {
            listener(data)
        }
    }
}