package com.example.bima.pariwisatayogyakarte.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GetorPostData {

    @GET("jsonBootcamp.php")
    fun listPariwisata() : Call<DataPariwisata>
}