package com.example.bima.pariwisatayogyakarte.view

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import com.example.bima.pariwisatayogyakarte.adapter.AdapterFavorite
import com.example.bima.pariwisatayogyakarte.helper.database
import com.example.bima.pariwisatayogyakarte.model.Favorite
import com.roger.catloadinglibrary.CatLoadingView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class FavoriteList: AppCompatActivity(), AnkoComponent<Context> {

    private var fav: MutableList<Favorite> = mutableListOf()
    private lateinit var recycler :RecyclerView
    private lateinit var adapter : AdapterFavorite
    private var cat : CatLoadingView = CatLoadingView()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(createView(AnkoContext.create(this)))
        adapter = AdapterFavorite(fav){}
        showList()


        recycler.adapter = adapter

    }

    fun showList(){
        cat.show(supportFragmentManager,"show")
            fav.clear()
            database.use {
                val result = select(Favorite.TABLE_NAME)
                val favorite = result.parseList(classParser<Favorite>())
                fav.addAll(favorite)
                adapter.notifyDataSetChanged()

            }
        cat.dismiss()
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(matchParent, wrapContent)
            orientation = LinearLayout.VERTICAL

                relativeLayout {
                    lparams(matchParent, wrapContent)
                    recycler = recyclerView {
                        lparams(matchParent, wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }
                }
        }
    }
}