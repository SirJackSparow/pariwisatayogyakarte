package com.example.bima.pariwisatayogyakarte.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import com.example.bima.pariwisatayogyakarte.R
import com.example.bima.pariwisatayogyakarte.helper.CheckConnection
import com.google.firebase.auth.FirebaseAuth
import com.roger.catloadinglibrary.CatLoadingView
import kotlinx.android.synthetic.main.login.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.startActivity

class Login : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private lateinit var cat: CatLoadingView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        cat =CatLoadingView()


        chechKoneksi()

        mAuth = FirebaseAuth.getInstance()


        registrasi.setOnClickListener {
            startActivity(Intent(this,Registration::class.java))
            finish()
        }
        login.setOnClickListener {
          login()
        }
        lupa.setOnClickListener {
          startActivity(Intent(this,ForgotPassword::class.java))
        }
    }

    private fun chechKoneksi(){
        var CheckConnection = CheckConnection.isNetworkOnline(this)
        if (!CheckConnection){
            Toast.makeText(this,"Periksa Koneksi Anda",Toast.LENGTH_LONG).show()
        }
    }

    override fun onStart() {
        super.onStart()
        if (mAuth?.currentUser != null){
            ctx.startActivity<MainActivity>()
            finish()
        }
    }

    private fun login(){
        cat.show(supportFragmentManager,"Loading")
        chechKoneksi()
        var email = email.text.toString()
        var pwd = Pwd.text.toString()


        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pwd)){

            mAuth!!.signInWithEmailAndPassword(email,pwd).addOnCompleteListener(this) { task ->
                if (task.isSuccessful){

                    val intent = Intent(this, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                    finish()
                }else{
                    Toast.makeText(this, "Check username atau Password", Toast.LENGTH_SHORT).show()
                    cat.dismiss()
                }
            }
        }
        else{
            Toast.makeText(this, "Kosong", Toast.LENGTH_SHORT).show()
            cat.dismiss()
        }
    }
}