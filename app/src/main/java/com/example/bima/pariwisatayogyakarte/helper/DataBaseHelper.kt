package com.example.bima.pariwisatayogyakarte.helper

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.bima.pariwisatayogyakarte.model.Favorite
import org.jetbrains.anko.db.*

class DataBaseHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "Favorite.db",null,1) {
    companion object {
        private var instance: DataBaseHelper? = null

        @Synchronized
        fun getInstance (ctx: Context): DataBaseHelper{
            if (instance == null ){
                instance = DataBaseHelper(ctx.applicationContext)
            }
            return instance as DataBaseHelper
        }
    }
    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(
            Favorite.TABLE_NAME,true
        ,Favorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            Favorite.NAMA to TEXT,
            Favorite.ALAMAT to TEXT,
            Favorite.DETAIL to TEXT,
            Favorite.GAMBAR to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(Favorite.TABLE_NAME,true)
    }
}

val Context.database : DataBaseHelper
         get() = DataBaseHelper.getInstance(applicationContext)